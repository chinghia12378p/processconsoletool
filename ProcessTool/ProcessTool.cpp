#pragma once
#include <iostream>
#include <Windows.h>
#include <string>
#include <io.h>
#include <fcntl.h>
#include <tlhelp32.h>
#include <Psapi.h>

using namespace std;

void WriteTitle();
void cls(HANDLE hConsole);
void Format(wstring &string, int size);
BOOL GetLocation(DWORD dwPID, wstring &location);
BOOL ViewProc();
BOOL KillProcByPID(DWORD dwPID);
BOOL CreatingProcess(wstring Filepath);

int main()
{
	_setmode(_fileno(stdout), _O_U16TEXT);
	wstring commander;
	wcout << "Hello this is simple C++ program to manage your process.";
	wcout << "Type <Show> to show all Process, <Kill> to close the process according to it's PID, <Start> to create new process, <Help> to see the usage, <Clear> to clear the screen and <Quit> to close this program.\n";
	do
	{
		wcout << ">>";
		getline(wcin, commander);
		if (commander == L"Help")
		{
			wcout << "Type <Show> to show all Process, <Kill> to close the process according to it's PID, <Start> to create new process, <Help> to see the usage, <Clear> to clear the screen and <Quit> to close this program.\n";
		}
		if (commander == L"Show") {
			WriteTitle();
			ViewProc();
		}
		if (commander == L"Kill") {
			DWORD PID;
			wcout << "The PID of process you want kill is: ";
			wcin >> PID;
			if (KillProcByPID(PID)) {
				wcout << "I killed it for you.\n";
				fflush(stdin);
			}
			else {
				wcout << "We don't have permission to do it.\n";
				fflush(stdin);
			}
		}
		if (commander == L"Start") {
			wstring path;
			wcout << "You must type directory of executable file you wan't run: ";
			getline(wcin, path);
			if (CreatingProcess(path)) {
				wcout << "I create new process successfully!\n";
			}
			else {
				wcout << "Nah, We don't have permission to do it.\n";
			}
		}
		if (commander == L"Clear") {
			HANDLE hStdout;
			hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
			cls(hStdout);
		}
	} while (commander != L"Quit");
}
void WriteTitle() {
	wstring titleName = L"Process Name";   //size of string is 70
	Format(titleName, 70);

	wstring titlePID = L"PID";             //size of string is 10
	Format(titlePID, 10);

	wstring titlePPID = L"PPID";		   //size of string is 10
	Format(titlePPID, 10);

	wstring titlePriBytes = L"Private Bytes"; //size of string is 15
	Format(titlePriBytes, 15);

	wstring titleWorkingSet = L"Working Set"; //size of string is 15
	Format(titleWorkingSet, 15);
	wstring titleLocation = L"Location"; //size of string is 50
	Format(titleLocation, 50);
	wcout << titleName << titlePID << titlePPID << titlePriBytes << titleWorkingSet << titleLocation << "\n";
}
BOOL ViewProc() {
	HANDLE hProcessSnap;
	HANDLE hProcess;
	PROCESSENTRY32 pe32;
	DWORD dwPriorityClass;

	// Take a snapshot of all processes in the system.
	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hProcessSnap == INVALID_HANDLE_VALUE)
	{
		return(FALSE);
	}

	// Set the size of the structure before using it.
	pe32.dwSize = sizeof(PROCESSENTRY32);

	// Retrieve information about the first process,
	// and exit if unsuccessful
	if (!Process32First(hProcessSnap, &pe32))
	{
		CloseHandle(hProcessSnap);          // clean the snapshot object
		return(FALSE);
	}

	do {
		wstring Filename = pe32.szExeFile;

		string sPID = to_string(pe32.th32ProcessID);
		wstring PID(sPID.begin(), sPID.end());

		string sPPID = to_string(pe32.th32ParentProcessID);
		wstring PPID(sPPID.begin(), sPPID.end());

		string sPriorBase = to_string(pe32.pcPriClassBase);
		wstring PriorBase(sPriorBase.begin(), sPriorBase.end());

		dwPriorityClass = 0;
		hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID);
		dwPriorityClass = GetPriorityClass(hProcess);
		CloseHandle(hProcess);

		//Get path of executables, Private bytes and Working Set
		PROCESS_MEMORY_COUNTERS_EX pmc;
		HANDLE Handle = OpenProcess(
			PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
			FALSE,
			pe32.th32ProcessID);
		TCHAR Buffer[MAX_PATH];
		unsigned long working_set = 0, private_bytes = 0;
		if (Handle)
		{
			GetModuleFileNameEx(Handle, 0, Buffer, MAX_PATH);
			if (GetProcessMemoryInfo(Handle, (PROCESS_MEMORY_COUNTERS *)&pmc, sizeof(pmc)))
			{
				working_set = pmc.WorkingSetSize / 1024;
				private_bytes = pmc.PrivateUsage / 1024;
			}
		}
		CloseHandle(Handle);

		wstring PriBytes = to_wstring(private_bytes) + L"K";
		wstring Working_set = to_wstring(working_set) + L"K";

		string sPriorClass = to_string(dwPriorityClass);
		wstring PriorClass(sPriorClass.begin(), sPriorClass.end());

		wstring location(Buffer);
		if (location.size() == 264) {
			location.replace(0, location.size(), L"");
		}
		//Format output
		Format(Filename, 70);
		Format(PID, 10);
		Format(PPID, 10);
		Format(PriBytes, 15);
		Format(Working_set, 15);
		Format(location, 50);
		wcout << Filename << PID << PPID << PriBytes << Working_set << location << "\n\n";
	} while (Process32Next(hProcessSnap, &pe32));
	CloseHandle(hProcessSnap);
	return(TRUE);
}
void Format(wstring &string, int size) {
	for (int i = string.size();i < size;i++) {
		string += L" ";
	}
}
BOOL GetLocation(DWORD dwPID, wstring &location) {
	HANDLE hModuleSnap = INVALID_HANDLE_VALUE;
	MODULEENTRY32 me32;

	// Take a snapshot of all modules in the specified process.
	hModuleSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwPID);
	if (hModuleSnap == INVALID_HANDLE_VALUE)
	{
		return(FALSE);
	}
	// Set the size of the structure before using it.
	me32.dwSize = sizeof(MODULEENTRY32);

	// Retrieve information about the first module,
	// and exit if unsuccessful
	if (!Module32First(hModuleSnap, &me32))
	{
		CloseHandle(hModuleSnap);           // show cause of failure
		return(FALSE);						// clean the snapshot object
	}
	location = me32.szExePath;
	return TRUE;
}
BOOL KillProcByPID(DWORD dwPID) {
	HANDLE explorer;
	explorer = OpenProcess(PROCESS_ALL_ACCESS, false, dwPID);
	BOOL res = TerminateProcess(explorer, 1);
	CloseHandle(explorer);
	return res;
}
/* Standard error macro for reporting API errors */
#define PERR(bSuccess, api){if(!(bSuccess)) printf("%s:Error %d from %s  on line %d\n", __FILE__, GetLastError(), api, __LINE__);}

void cls(HANDLE hConsole)
{
	COORD coordScreen = { 0, 0 };    /* here's where we'll home the
										cursor */
	BOOL bSuccess;
	DWORD cCharsWritten;
	CONSOLE_SCREEN_BUFFER_INFO csbi; /* to get buffer info */
	DWORD dwConSize;                 /* number of character cells in
										the current buffer */

										/* get the number of character cells in the current buffer */

	bSuccess = GetConsoleScreenBufferInfo(hConsole, &csbi);
	PERR(bSuccess, "GetConsoleScreenBufferInfo");
	dwConSize = csbi.dwSize.X * csbi.dwSize.Y;

	/* fill the entire screen with blanks */

	bSuccess = FillConsoleOutputCharacter(hConsole, (TCHAR) ' ',
		dwConSize, coordScreen, &cCharsWritten);
	PERR(bSuccess, "FillConsoleOutputCharacter");

	/* get the current text attribute */

	bSuccess = GetConsoleScreenBufferInfo(hConsole, &csbi);
	PERR(bSuccess, "ConsoleScreenBufferInfo");

	/* now set the buffer's attributes accordingly */

	bSuccess = FillConsoleOutputAttribute(hConsole, csbi.wAttributes,
		dwConSize, coordScreen, &cCharsWritten);
	PERR(bSuccess, "FillConsoleOutputAttribute");

	/* put the cursor at (0, 0) */

	bSuccess = SetConsoleCursorPosition(hConsole, coordScreen);
	PERR(bSuccess, "SetConsoleCursorPosition");
	return;
}
BOOL CreatingProcess(wstring Filepath) {
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));
	LPWSTR path = &Filepath[0];
	if (!CreateProcess(NULL,   // No module name (use command line)
		path,        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi)           // Pointer to PROCESS_INFORMATION structure
		)
	{
		printf("CreateProcess failed (%d).\n", GetLastError());
		return false;
	}
	return true;
}